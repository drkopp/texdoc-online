// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.html

import kotlinx.html.*
import kotlinx.html.dom.append
import kotlinx.html.dom.createHTMLDocument
import org.islandoftex.texdoconline.Session
import org.w3c.dom.Document
import org.w3c.dom.Element
import java.io.File

object DOMBuilder {
    /**
     * Create the basic page layout for all pages. This especially includes
     * menus and footer.
     */
    @Suppress("MaxLineLength", "LongMethod")
    private fun getDocumentStructure() = createHTMLDocument().html {
        lang = "en"
        head {
            title("texdoc online documentation")
            meta("description", "An online documentation system for TeX Live")
            meta("charset", "UTF-8")
            meta {
                httpEquiv = "X-UA-Compatible"
                content = "IE=edge"
            }

            link(href = "/css/cirrus.min.css", type = "text/css", rel = "stylesheet")
            link(href = "/css/texdoc.css", type = "text/css", rel = "stylesheet")

            val fontCSS = Session.resourceDirectory?.resolve("css/fonts.css")
                ?: File("css/fonts.css")
            if (fontCSS.exists() && fontCSS.isFile) {
                link(href = "/css/fonts.css", type = "text/css", rel = "stylesheet")
            } else {
                link(href = "https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700", rel = "stylesheet")
                link(href = "https://fonts.googleapis.com/css?family=Montserrat:400,700", rel = "stylesheet")
            }
        }
        body {
            div("header header-fixed unselectable header-animated") {
                div("header-brand") {
                    div("nav-item no-hover") {
                        a("/") {
                            h6("title") { +"texdoc" }
                        }
                    }

                    // get some more spacing
                    div("nav-item nav-btn") {
                        id = "header-btn"
                        span()
                        span()
                        span()
                    }
                }
                div("header-nav") {
                    id = "header-menu"
                    div("nav-left") {
                        div("nav-item text-center") {
                            a("https://gitlab.com/islandoftex/images/texdoc-online") {
                                span("iconify") {
                                    attributes["data-icon"] = "la:gitlab"
                                    attributes["data-icon-inline"] = "true"
                                }
                            }
                            a("https://gitter.im/Island-of-TeX/community") {
                                span("iconify") {
                                    attributes["data-icon"] = "la:gitter"
                                    attributes["data-icon-inline"] = "true"
                                }
                            }
                            a("https://matrix.to/#/!titTeSvZiqNOvRIKCv:matrix.org?via=matrix.org") {
                                span("iconify") {
                                    attributes["data-icon"] = "cib:matrix"
                                    attributes["data-icon-inline"] = "true"
                                }
                            }
                            a("https://gitlab.com/islandoftex") {
                                span("iconify") {
                                    attributes["data-icon"] = "la:map-solid"
                                    attributes["data-icon-inline"] = "true"
                                }
                            }
                        }
                    }
                    div("nav-right") {
                        div("nav-item") {
                            a("/index.html#section-api") {
                                span("iconify") {
                                    attributes["data-icon"] = "la:code-solid"
                                    attributes["data-icon-inline"] = "true"
                                }
                                span("icon-pad")
                                +" Our API"
                            }
                            a("/index.html#section-deploy") {
                                span("iconify") {
                                    attributes["data-icon"] = "la:server-solid"
                                    attributes["data-icon-inline"] = "true"
                                }
                                span("icon-pad")
                                +" Deploying your own instance"
                            }
                        }
                    }
                }
            }
            div("content avoid-header") {
                id = "texdoconline-body"
            }
            footer("footer") {
                div("content") {
                    id = "version-container"
                }
                p("u-text-center blue-text") {
                    +"Made with "
                    span("iconify animated pulse large") {
                        attributes["data-icon"] = "la:heart-solid"
                        attributes["data-icon-inline"] = "true"
                    }
                    +" by the Island of "
                    +"T"
                    sub { +"E" }
                    +"X"
                    +"."
                }
            }

            script(type = "application/javascript", src = "/js/texdoc-online.js") {
                attributes["crossorigin"] = "anonymous"
                // will always suceed because we bundle texdoc-online.js from resources
                integrity = ScriptManager.getIntegrityHashForScript("js/texdoc-online.js")
            }
            try {
                script(type = "application/javascript", src = "/js/iconify.min.js") {
                    attributes["crossorigin"] = "anonymous"
                    // will fail with IllegalArgumentException if we do not have iconify.min.js
                    // locally available
                    integrity = ScriptManager.getIntegrityHashForScript("js/iconify.min.js")
                    defer = true
                }
            } catch (_: IllegalArgumentException) {
                script(type = "application/javascript", src = "https://code.iconify.design/1/1.0.7/iconify.min.js") {
                    attributes["crossorigin"] = "anonymous"
                    integrity = "sha512-zX8zoJnq7690zbKYSSCHh1UNVeBVqfjc083+kPwybKV9YI9R+Mr7j4G9TrgcgxmWHqJz1QwL6x5+E4tfWhxSUw=="
                    defer = true
                }
            }
        }
    }

    /**
     * Assemble a custom page by inserting [builder] into the main page content
     * which is empty by default.
     */
    fun assembleCustomPage(builder: TagConsumer<Element>.() -> Unit): Document =
        getDocumentStructure().apply {
            getElementById("texdoconline-body").append(builder)
        }
}
