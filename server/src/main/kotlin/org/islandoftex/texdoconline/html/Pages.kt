// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.html

import kotlinx.html.*
import kotlinx.html.dom.serialize
import org.islandoftex.texdoconline.tex

/**
 * Provide HTML pages to be served.
 */
object Pages {
    /**
     * The content of the missing page (i.e. error 404 handler).
     */
    val missingPage = DOMBuilder.assembleCustomPage {
        div("placeholder") {
            div("placeholder-icon") {
                span("iconify animated pulse large") {
                    attributes["data-icon"] = "noto:duck"
                    attributes["data-icon-inline"] = "true"
                    style = "font-size:500%;"
                }
            }
            h6("placeholder-title") { +"Oh no" }
            div("placeholder-subtitle") {
                +"We cannot find the page you are looking for"
            }
        }
    }.serialize()

    /**
     * The content of the index page.
     */
    val mainPage = DOMBuilder.assembleCustomPage {
        p("u-text-center") {
            +"Welcome to the "
            tex()
            +" and "
            span("latex") {
                +"L"
                sup { +"a" }
                +"T"
                sub { +"e" }
                +"X"
            }
            +" documentation lookup system."
        }

        div("card") {
            div("card-head") {
                p("card-head-title") {
                    +"Documentation lookup"
                }
            }
            div("content form-section avoid-card-title") {
                div ("input-control") {
                    textInput(classes = "input-contains-icon input-control--pilled") {
                        id = "searchquery"
                        placeholder = "type the package name..."
                    }
                    span("icon") {
                        span("iconify") {
                            attributes["data-icon"] = "la:file-alt"
                            attributes["data-icon-inline"] = "true"
                        }
                    }
                }
                div("level-right") {
                    button(classes = "btn") {
                        id = "querySubmit"
                        +"Submit query "
                        span("iconify") {
                            attributes["data-icon"] = "la:chevron-circle-right-solid"
                            attributes["data-icon-inline"] = "true"
                        }
                    }
                }
            }
            div("provide-bar") {
                id ="searchresults"
            }
        }

        div("card") {
            div("card-head") {
                div("row card-head-title") {
                    p("col-8") {
                        id = "topic-header"
                        +"Topics"
                    }
                    button(classes = "btn btn-small col-2") {
                        id = "fetchAllTopicButton"
                        +"All "
                        span("iconify") {
                            attributes["data-icon"] = "la:caret-right-solid"
                            attributes["data-icon-inline"] = "true"
                        }
                    }
                    button(classes = "btn btn-small col-2") {
                        id = "fetchTopicButton"
                        +"Promoted "
                        span("iconify") {
                            attributes["data-icon"] = "la:caret-right-solid"
                            attributes["data-icon-inline"] = "true"
                        }
                    }
                }
            }
            div {
                id = "topics"
            }
        }

        div("divider")

        span("anchor") {
            id = "section-api"
        }
        section {
            h2("title") { +"Our API" }
            p {
                +"You are visiting the front-end for the "
                tex()
                +"doc online software. In the backend, it is running a RESTful API that provides a few "
                +"endpoints for you to use. Please make sure you read the following instructions about "
                +"what to expect and what not to."
            }
            p {
                +"Each of these requests will return either HTTP status code 200 (OK) or, in the case of "
                +"any error, HTTP status code 422 (Unprocessable Entity). The "
                code("api-description") {
                    +"/version"
                }
                +"endpoint is guaranteed not to fail."
            }

            table("table") {
                thead {
                    tr {
                        th(classes = "left-align") { +"Endpoint" }
                        th(classes = "left-align") { +"Description" }
                    }
                }
                tbody {
                    tr {
                        td("left-align") {
                            code("api-description") {
                                +"/version"
                            }
                        }
                        td("left-align") {
                            +"This endpoint returns the versions of the API and data format ("
                            code("api-description") { +"api" }
                            +"), the installed version of texdoc ("
                            code("api-description") { +"texdoc" }
                            +") and the date of the last "
                            tex()
                            +" Live update as ISO date string ("
                            code("api-description") { +"tlpdb" }
                            +"). Make sure your client software always expects the correct API "
                            +"version to avoid problems. Our API versions follow semantic versioning "
                            +"with all its consequences."
                        }
                    }
                    tr {
                        td("left-align") {
                            code("api-description") {
                                +"/texdoc/〈name〉"
                            }
                        }
                        td("left-align") {
                            +"On this path, the list of entries is returned that a local call to "
                            code("api-description") { +"texdoc -l" }
                            +"would result in. For each entry, there are two fields:"
                            ul {
                                li {
                                    code("api-description") { +"path" }
                                    +" containing the path to the documentation file relative to the "
                                    code("api-description") { +"doc" }
                                    +" subfolder of the "
                                    code("api-description") { +"TEXMFDIST" }
                                    +" directory."
                                }
                                li {
                                    code("api-description") { +"description" }
                                    +" containing the file's description if existent (empty otherwise)."
                                }
                            }
                            +"The application will always return a JSON array of such entries."
                        }
                    }
                    tr {
                        td("left-align") {
                            code("api-description") {
                                +"/serve/〈name〉/〈index〉"
                            }
                        }
                        td("left-align") {
                            +"This call results in the file corresponding to the documentation file at index "
                            code("api-description") { +"〈index〉" }
                            +" of the result of "
                            code("api-description") { +"/texdoc/〈name〉" }
                            +" being responded to the client."
                        }
                    }
                    tr {
                        td("left-align") {
                            code("api-description") {
                                +"/pkg/〈name〉"
                            }
                        }
                        td("left-align") {
                            +"This endpoint is actually a shortcut to the "
                            code("api-description") { +"/serve/〈name〉/0" }
                            +" endpoint previously introduced to preserve compatibility with the API of texdoc.net."
                        }
                    }
                    tr {
                        td("left-align") {
                            code("api-description") {
                                +"/topics/list"
                            }
                        }
                        td("left-align") {
                            +"This endpoint returns the list of topics known to the application specified by their "
                            code("api-description") { +"key" }
                            +" and a caption called "
                            code("api-description") { +"details" }
                            +". This is a direct interface to CTAN's API for topics. Network access for the server "
                            +"software is required."
                        }
                    }
                    tr {
                        td("left-align") {
                            code("api-description") {
                                +"/topic/〈name〉"
                            }
                        }
                        td("left-align") {
                            +"This endpoint returns details for a topic by returning the "
                            code("api-description") { +"key" }
                            +" (what is passed in as "
                            code("api-description") { +"〈name〉" }
                            +"), a string with a short description called "
                            code("api-description") { +"details" }
                            +" and a list of package names (strings) called "
                            code("api-description") { +"packages" }
                            +". This is a direct interface to CTAN's API for topics. Network access for the server "
                            +"software is required."
                        }
                    }
                }
            }
        }

        span("anchor") {
            id="section-deploy"
        }
        section {
            h2("title") { +"Deploying your own instance" }
            h3("title") { +"Running an executable JAR file" }
            p {
                tex()
                +"doc online is an open-source product available at its "
                a("https://gitlab.com/islandoftex/images/texdoc-online") { +"GitLab repository page" }
                +". It consists of one server component that does host the API and the front-end "
                +"at the same time. If you simply want to have these features available to you, "
                +"download the latest build artifacts from there (i.e. the JAR file with dependencies) "
                +"and run it with your local Java installation. Please note that the software requires a local "
                tex()
                +" distribution installed and internet connection for fetching the topics."
            }
            p {
                +"When running the JAR file, a local webserver will start up and host the service at "
                code("api-description") { +"127.0.0.1:8080" }
                +"which is configurable through the command-line switches. Calling this address will "
                +"open up the web interface. Appending API paths will execute the API calls and return "
                +"the JSON objects. To stop the server from running, simply "
                kbd("typewriter") { +"CTRL" }
                +"+"
                kbd("typewriter") { +"C" }
                +"the Java process."
            }
            h3("title") { +"Booting up a container" }
            p {
                +"Alternatively, the preferred way to host your own instance should be running our Docker "
                +"container (a list of available tags may be found"
                a("https://gitlab.com/islandoftex/images/texdoc-online/container_registry") { +"at GitLab" }
                +"). The Docker container runs the web service as its entrypoint so running the server will "
                +"start up the web server . Because the Docker container is built on top of our "
                tex()
                +"Live images, it does not require a local "
                tex()
                +" installation which might be beneficial for web servers. Internet connection is required,"
                +"though. This all comes at the cost of being quite a big docker image of some gigabytes."
            }
            p {
                +"If your Docker container runs for multiple days, it features an automated update of "
                tex()
                +"Live. Every day at 00:01 in the morning, a full update of the distribution is performed "
                +"updating to the latest and greatest package versions "
                tex()
                +" Live has to offer. Please note, that updates between distribution releases will not be "
                +"performed that way; in order to upgrade from TL&nbsp;2020 to TL&nbsp;2021 you would need "
                +"to pull the latest Docker image again but that means that the need for re-pulling the Docker "
                +"image reduces to once per year."
            }
        }
    }.serialize()
}
