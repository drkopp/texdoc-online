// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.versionOption
import com.github.ajalt.clikt.parameters.types.file
import com.github.ajalt.clikt.parameters.types.int
import io.ktor.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.util.*

class CLI : CliktCommand(name = "texdoc-online") {
    private val host by option("-h", "--host", help = "The host to bind listening to.").default("0.0.0.0")
    private val port by option("-p", "--port", help = "The port to listen on (unencrypted).").int().default(8080)
    private val resourceDirectory by option(
        "-s",
        "--serve",
        help = "A directory to serve content from (/js/texdoc-online.js will be made available by the server)."
    ).file(
        mustExist = true,
        canBeFile = false,
        canBeDir = true,
        mustBeWritable = false,
        mustBeReadable = true
    )

    override fun run() {
        resourceDirectory?.let {
            println("texdoc online will watch the following include directory: ${it.absolutePath}")
            Session.resourceDirectory = it
        }
        embeddedServer(Netty, host = host, port = port, module = Application::module).start()
    }
}

/**
 * The application's entry point starting the web server.
 */
@KtorExperimentalAPI
fun main(args: Array<String>) = CLI()
    .versionOption(CLI::class.java.`package`.implementationVersion ?: "DEVELOPMENT")
    .main(args)
