// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.model

import kotlinx.serialization.Serializable

/**
 * This is the representation of the essentials of an entry texdoc outputs.
 * It ignores the score on purpose.
 */
@Serializable
public data class InternalTeXdocEntry(
    /**
     * The file path (or a portion thereof) needed to locate the file within the texmf tree.
     */
    val path: String,
    /**
     * The description of the documentation file if any.
     */
    val description: String
)
