// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.build

object Versions {
    // plugin dependencies
    const val detekt = "1.14.2"
    const val dokka = "1.4.20"
    const val kotlin = "1.4.21"
    const val shadow = "6.1.0"
    const val spotless = "5.8.2"
    const val spotlessChangelog = "2.0.0"
    const val versionsPlugin = "0.36.0"

    // non-plugin dependencies
    const val clikt = "3.0.1"
    const val coroutines = "1.4.2"
    const val ctanapi = "0.1.0"
    const val datetime = "0.1.1"
    const val html = "0.7.2"
    const val kotest = "4.3.1"
    const val ktor = "1.4.1"
    const val serialization = "1.0.1"
    const val slf4j = "1.7.30"
    const val texdocapi = "1.0.0"
}
