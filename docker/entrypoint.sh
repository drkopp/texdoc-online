#!/bin/bash
# start texdoc online
java -jar /texdoconline/*with-deps-*.jar "$@" &

# set up a cron job to run every day on 1 minute past midnight
echo "@daily /texdoconline/tlupdate.sh" > /tmp/cron.txt

# start cron in foreground mode
supercronic /tmp/cron.txt
