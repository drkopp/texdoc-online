repositories {
    jcenter()
}

plugins {
    val versions = org.islandoftex.texdoconline.build.Versions
    kotlin("multiplatform") version versions.kotlin apply false
    kotlin("plugin.serialization") version versions.kotlin apply false
    id("com.diffplug.spotless-changelog") version versions.spotlessChangelog
    id("com.github.ben-manes.versions") version versions.versionsPlugin
    id("io.gitlab.arturbosch.detekt") version versions.detekt
    id("com.diffplug.spotless") version versions.spotless
}

spotlessChangelog {
    setAppendDashSnapshotUnless_dashPrelease(false)
    tagPrefix("v")
    commitMessage("Release v{{version}}")
    remote("origin")
    branch("master")
}

group = "org.islandoftex.texdoconline"
description = "A web server serving online TeX documentation"
version = project.spotlessChangelog.versionNext

tasks.named<com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask>("dependencyUpdates") {
    resolutionStrategy {
        componentSelection {
            all {
                fun isNonStable(version: String) = listOf("alpha", "beta", "rc", "cr", "m", "preview", "b", "ea")
                        .any { qualifier ->
                            version.matches(Regex("(?i).*[.-]$qualifier[.\\d-+]*"))
                        }
                if (isNonStable(candidate.version) && !isNonStable(currentVersion)) {
                    reject("Release candidate")
                }
            }
        }
    }
    checkForGradleUpdate = false
}

spotless {
    kotlinGradle {
        target(
            "build.gradle.kts", "buildSrc/build.gradle.kts",
            "client/build.gradle.kts", "server/build.gradle.kts",
            "shared/build.gradle.kts"
        )
        trimTrailingWhitespace()
        endWithNewline()
    }
    kotlin {
        licenseHeader("// SPDX-License-Identifier: BSD-3-Clause")
        target("buildSrc/src/**/*.kt", "client/src/**/*.kt", "server/src/**/*.kt", "shared/src/**/*.kt")
        trimTrailingWhitespace()
        endWithNewline()
    }
}

detekt {
    failFast = false
    buildUponDefaultConfig = true
    config = files("detekt-config.yml")
    input = files(
        "client/src/main/kotlin",
        "server/src/main/kotlin",
        "shared/src/main/kotlin"
    )
}

subprojects {
    version = rootProject.version
    group = rootProject.group
    description = rootProject.description

    repositories {
        jcenter()
        maven("https://dl.bintray.com/kotlin/kotlinx")
        maven("https://dl.bintray.com/kotlin/kotlin-eap")
        maven("https://dl.bintray.com/kotlin/kotlin-dev")
    }
}
